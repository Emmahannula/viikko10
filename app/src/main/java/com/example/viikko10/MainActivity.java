package com.example.viikko10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {

    WebView web;
    EditText syöte;
    String URL=null;
    ArrayList history;
    ListIterator LI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        web = (WebView) findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        syöte = findViewById(R.id.editText);
        history = new ArrayList<String>();
    }

    public void Load(View v) {
        LoadURL();
    }

    public void LoadURL() {
        String sivu = syöte.getText().toString();
        if (sivu.equals("index.html")) {
            URL = "file:///android_asset/index.html";
        }
        else {
            URL = "http://" + sivu;
        }
        history.add(URL);
        if (history.size() == 11) {
            history.remove(0);
        }
        LI = history.listIterator(history.size());

        web.loadUrl(URL);
    }

    public void Refresh(View v) {
        web.loadUrl(URL);
    }

    public void executeJS(View v) {
        web.evaluateJavascript("shoutOut()", null);
    }

    public void returnJS(View v) {
        web.evaluateJavascript("initialize()", null);
    }

    public void nextURL(View v) {
        if (LI.hasNext()) {
            URL = LI.next().toString();
            web.loadUrl(URL);
        }
    }

    public void previousURL(View v) {
        if (LI.hasPrevious()) {
            System.out.println("edellinen olemassa");
            URL = LI.previous().toString();
            web.loadUrl(URL);
        }
    }
}
